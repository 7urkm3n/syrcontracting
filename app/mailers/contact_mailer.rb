class ContactMailer < ApplicationMailer
	default to: "ayhansayer@hotmail.com"
	# default from: 'no-reply@brrcr.com'

	def contact_email(name, email, message)
		@full_name = name
		@email = email
		@message = message

		mail(from: email, subject:"Contact Form Message")
	end
end