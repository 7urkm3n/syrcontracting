class ApplicationMailer < ActionMailer::Base
  # default from: 'no-reply@brrcr.com'
  layout 'mailer'
end
