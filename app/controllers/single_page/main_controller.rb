class SinglePage::MainController < ApplicationController
	layout "single_page"

	def index
	end

	def contact_us
		@contact = Contact.new(contact_params)
		@contact.status = false

		respond_to do |format|
	      if @contact.save
	      		# email
  				name = params[:contact][:full_name]
				email = params[:contact][:email]
				message = params[:contact][:message]
				ContactMailer.contact_email(name, email, message).deliver

	        format.js
	      else
	        format.js
	      end
	    end
	end
private

    def contact_params
      params.require(:contact).permit(:full_name, :email, :message)
    end
end
