Rails.application.routes.draw do
  
  # get 'order-now' => 'static_pages#order_now'

  namespace :single_page, path: '' do
    post 'contact_us' => 'main#contact_us'
    # get '/menu'     => "home#menu"
    # get '/specials' => "home#specials"
    # get '/about'    => "home#about_us"
    # get '/contact'  => "home#contact"
  	# resources :projects, only: [:index, :show]

    # resources :contacts, only: [:create]
  end
  
  root 'single_page/main#index'
end
